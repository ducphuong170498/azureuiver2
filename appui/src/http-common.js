import axios from "axios";

export default axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL || 'https://api.phuongvd.click/api',
  headers: {
    "Content-type": "application/json"
  }
});